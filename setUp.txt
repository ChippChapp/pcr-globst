MODEL SET-UP FOR PCR-GLOBST

Description of data required to set model up at 10km resolution
Due to finer spatial resolution, model set-up limit first to Mississippi and -if necessary- 

Required input data
	DEM -> use data from PCR-GLOBWB 2.0
	LDD -> same
	DISTANCE TO RIVER -> don't know whether PCR-GLOBWB 2.0 still has it; otherwise take from 1.0 or maybe Gena has information
	PRECIPITATION -> extract from PCR-GLOBWB 2.0; monthly resolution
	TEMPERATURE -> same
	HWSD -> available at 30'', i.e. upscale to correct resolution
	NDVI -> monthly resolution
	LAND USE -> GLC SHARE at 1km, i.e. upscale to correct resolution
	