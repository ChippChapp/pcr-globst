# -*- coding: utf-8 -*-
"""
Created on Fri May 08 21:34:05 2015

@author: hoch
"""
import os
from pcraster import *

def calc_Kannual(self, Texture0, Texture1, Texture2, Texture3, OM, Fclay, Fsand, Fsilt, outputdir):

    """
    The annual soil erodibility relies strongly on soil properties such as texture or clay fraction.
    Depening on these factors, soil may either be easily to be eroded or not.
    """

    Kr0 = ifthenelse(Texture0, scalar(1.44), scalar(0))
    Kr1 = ifthenelse(Texture1, scalar(1.17), scalar(0))
    Kr2 = ifthenelse(Texture2, scalar(1.44), scalar(0))
    Kr3 = ifthenelse(Texture3, scalar(4.50), scalar(0))
    self.KfactorRATIO = Kr0+Kr1+Kr2+Kr3

    OMC = OM/Fclay
    Dg = -3.5*Fsand-2.0*Fsilt-0.5*Fclay
    EXP = (-0.0021*OMC-0.00037*(OMC)**2-4.02*Fclay+1.12*Fclay**2);
    KfactorYEAR = 0.0293*(0.65-Dg+0.24*Dg**2)**EXP
    self.KfactorYEAR=cover(KfactorYEAR,mapminimum(KfactorYEAR))

    self.report(self.KfactorYEAR, os.path.join(outputdir, 'Kannual'))

    print 'annual K-factor successfully computed!'

    return

def calc_Kmonthly(self, Tmax, Tmin, SMT, KfactorRATIO, KfactorYEAR, timeStep, outputdir):

    """
    Since fluctuations in especially temperature can strongly increase soil erodibility,
    it has to be taken into account as well.
    """

    MTfr = ifthenelse(Tmax >= SMT, (Tmax-SMT)/(Tmax-Tmin), 0)
    MTfr = max(0, MTfr)
    MTfr = min(1, MTfr)

    self.KfactorMONTH = self.KfactorRATIO*MTfr*self.KfactorYEAR + self.KfactorYEAR*(1-MTfr)

    self.report(self.KfactorMONTH, os.path.join(outputdir, 'K_month'))

    print 'monthly K-factor successfully computed for timestep', timeStep

    return
