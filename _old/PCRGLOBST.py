# -*- coding: utf-8 -*-
"""
Created on Fri May 08 2015

PCR-GLOBST
THis model computes sediment transport into the oceans globally at 30' resolution.
It employs the RUSLE approach and hence models long-term results with monthly temporal resolution.
This is as well represented by the use of long-term monthly averages.

The model consists of three main parts:
First, soil loss calculated by means of RUSLE.
Second, delivery ratio of eroded soil.
Third, erosion and sediment transport into the oceans. This is achieved multiplying soil loss and delivery ratio and subsequently routing it along a LDD.

@author: J.M. Hoch, Departement of Physical Geography, Utrecht University, The Netherlands
         former: Departement of Civil Engineering and Management, University of Twente, The Netherlands
"""

from pcraster import *
from pcraster.framework import *
import os, sys
import matplotlib.pyplot as plt
import numpy as np

### Load all required data and modules of model
from load_data import *
from SoilLossModule import *
from DeliveryRatioModule import *
from HillslopeDeliveryModule import *

class timeseries(object):
    '''Class holding the information for a timeseries'''
    
    def __init__(self,name):
        """requires as input the name of the variable stored""" 
        self.name= name
        self.sampledTSS= np.array([])
        self.ID= np.array([])
        self.sampledTSS= np.array([])
        
    def update(self, ID, tssValues):
        #-init IDs
        tssValues.shape= (1,-1)
        if self.ID.size == 0:
            self.ID= ID
        #-check IDs
        if np.any(ID != self.ID):
            sys.exit('IDs passed do not match')
        #-add values
        if self.sampledTSS.size == 0:
            self.sampledTSS= tssValues
        else:
            self.sampledTSS= np.vstack((self.sampledTSS,tssValues))
            
    def report(self,outputDir, formatStr= '%f'):
        fileName= os.path.join(outputDir,'%s.txt' % self.name.lower())
        print 'writing tss output to %s' % fileName
        txtFile= open(fileName,'w')
        formatStr= ' '+formatStr+','
        wStr= 'ID:'
        for ID in self.ID:
            wStr+= ' %d,' % ID
        wStr+= '\n'
        txtFile.write(wStr)
        for timeStep in xrange(self.sampledTSS.shape[0]):
            wStr= '%d:' % (timeStep+1)
            for value in self.sampledTSS[timeStep,:]:
                wStr+= formatStr % value
            wStr+= ' \n'            
            txtFile.write(wStr)
        txtFile.close()
        #/*end of class*/

def timeOutputViaNumpy(locationMap, valueMap, MV= -999.9):
    """Returns a tuple of two arrays: 1) unique ids from location map, 2) corresponding values """
    locations= pcr2numpy(locationMap,0)
    locIDs= np.unique(locations[locations > 0])
    values= pcr2numpy(valueMap,MV)[locations > 0]
    sortIndex= np.argsort(locIDs)
    return locIDs[sortIndex], values[sortIndex]

### Provide paths of working-, input- and outputdirectory
workdir = os.path.abspath('D:\hoch\PCR-GLOBST')
inputdir = os.path.abspath('D:\hoch\PCR-GLOBST\input')
outputdir = os.path.abspath('D:\hoch\PCR-GLOBST\output')

class RUSLE(DynamicModel):
  def __init__(self):
    DynamicModel.__init__(self)
    setclone('globalclone.map')

  def initial(self):
           
    ### ROUTING OPTIONS
    # if set to 'true', accufractionflux instead of accuflux will be executed
    self.accufractionflux = False
    # define fraction to be transported in accufractionflux (between 0 and 1)
    self.accufraction = 0.9
 
    ### MODEL OPTIONS
    # incorporate the P-factor as well or not?    
    self.apply_Pfactor = False
    # setting constants
    self.SMT = scalar(-0.00)
    self.Manning_n2 = scalar(0.049)
    self.alpha = scalar(9.53)
    self.beta = scalar(0.79)
    self.theta = scalar(2)
    self.zeta = scalar(1)
    OM = scalar(1)
    print 'constants are defined'
    # setting initial value
    self.ErosionTOT = scalar(0)
      
    print 'INITIAL SECTION HAS STARTED'  
    
    # importing input data    
    load_input_initial(self, inputdir, workdir)

    # calculating factors relevant for Soil Loss Module    
    self.LS = calc_LS(self.slopeGRA, self.gamma_TBL, self.slopeLength)
    self.KfactorYEAR, self.KfactorRATIO = calc_Kannual(self.Texture0, self.Texture1, self.Texture2, self.Texture3, OM, self.Fclay, self.Fsand, self.Fsilt)
    self.RfactorYEAR = calc_Rannual(self.Pannual)
    self.Pfactor = calc_P(self.slopeGRA, self.apply_Pfactor)    
    
    # calculating factors relevant for Delivery Ratio Module    
    self.Manning_n1 = Manning_n1(self.slopeGRA, self.Manning_n1_TBL)
    self.Manning_n4 = Manning_n4(self.barrensparse,self.closedshrub,self.crop,self.decbroad,self.decneedle,self.evergrbroad,self.evergrneedle,
               self.grass,self.mixforests,
               self.openshrub,self.permanentwet,self.savannas,self.snowice,self.urban,self.water,self.woodysavannas) 
    #-timeseries to be reported
    self.timeSeries= {}
    self.formats= {}
    self.timeSeries['SedTransport']= timeseries('SedTransport')
    self.formats['SedTransport']= '%.1f'
    self.timeSeries['Erosion']= timeseries('Erosion')
    self.formats['Erosion']= '%.3e'
    
    #-report maps    
    self.report(self.LS,os.path.join(outputdir, 'LSfactor')) 
    if self.apply_Pfactor == True:
        self.report(self.Pactor,os.path.join(outputdir, 'Pfactor'))
    
    print 'INITIAL SECTION TERMINATED'

  def dynamic(self):
      
    timeStep=self.currentTimeStep()
    
    if timeStep == 1:
        print 'DYNAMIC SECTION HAS BEEN STARTED'
        if self.accufractionflux == False:
            print 'CHOSEN ROUTING FUNCTION: ACCUFLUX'
        else:
            print 'CHOSEN ROUTING FUNCTION： ACCUFRACTIONFLUX'
            print 'FRACTION: ',self.accufraction
      
    # importing input data  
    load_input_dynamic(self, inputdir, workdir, timeStep)
    
    # calculating factors relevant for Soil Loss Module    
    self.RfactorMONTH = calc_Rmonthly(self.Precip, self.Pannual, self.RfactorYEAR, timeStep)    
    self.KfactorMONTH = calc_Kmonthly(self.Tmax, self.Tmin, self.SMT, self.KfactorRATIO, self.KfactorYEAR, timeStep)
    self.CfactorMONTH = calc_C(self.theta, self.zeta, self.NDVI, timeStep)    
    # calculating Soil Loss
    self.SoilLoss = calc_SoilLoss(self.LS, self.RfactorMONTH, self.KfactorMONTH, self.CfactorMONTH, self.Pfactor, self.CellArea, timeStep)    
    
    # calculating factors relevant for Delivery Ratio Module    
    self.Manning_v3 = Manning_v3(self.NDVI)
    self.Manning = calc_Manning(self.Manning_n1, self.Manning_n2, self.Manning_v3, self.Manning_n4, timeStep)
    self.HydroCoeff = calc_HydroCoeff(self.Qsurface, self.Precip, timeStep)
    # calculating Delivery Ratio
    self.DeliveryRatio = calc_DeliveryRatio(self.alpha, self.beta, self.HydroCoeff, self.slopeGRA, self.Manning, self.slopeLength, timeStep)
    
    # calculating Hillslope Delivery and sediment transport into oceans
    self.SedTransport, self.Erosion=\
        calc_SedTransport(self.SoilLoss, self.DeliveryRatio, self.ErosionTOT, self.LDD, self.accufraction, self.accufractionflux, self.RiverLOCS, timeStep)
       
    ##-report
    #-maps    
    self.report(self.RfactorMONTH,os.path.join(outputdir, 'Rfactor'))
    self.report(self.KfactorMONTH,os.path.join(outputdir, 'Kfactor'))
    self.report(self.CfactorMONTH,os.path.join(outputdir, 'Cfactor'))    
    self.report(self.SoilLoss,os.path.join(outputdir,'SoilLoss'))
    self.report(self.HydroCoeff,os.path.join(outputdir,'HydrCoef'))
    self.report(self.DeliveryRatio,os.path.join(outputdir, 'DelRatio'))
    if accufractionflux == False:
        self.report(self.SedTransport,os.path.join(outputdir,'ST_acflux'))
    else:
        self.report(self.SedTransport,os.path.join(outputdir,'ST_acfrac'))
    #-timeseries
    for name in self.timeSeries.keys():
        ID, tssValues= timeOutputViaNumpy(self.RiverLOCS,getattr(self,name))
        self.timeSeries[name].update(ID,tssValues)

    if timeStep == 12:
        print 'HURRAY, MODEL IS FINISHED!!!'
        for name in self.timeSeries.keys():
            self.timeSeries[name].report(outputdir, self.formats[name])
        
nrOfTimeSteps= 12
myModel = RUSLE()
dynamicModel = DynamicFramework(myModel,nrOfTimeSteps)
dynamicModel.run()     
    
     
    
    
    
        
    
    
    

    
        