# -*- coding: utf-8 -*-
"""
Created on Fri May 08 21:37:45 2015

@author: hoch
"""

from pcraster import *
import os

def calc_Rannual(self, Pannual, outputdir):

    """
    The R-factor represents the rainfall-runoff erosivity which is the impact of raindrops on soil.
    Similar as for the K-factor, the monthly values are derived from a annual value which in turn is based
    on the total annual sum of precipitation.
    Thereby R increases strongly after a threshold of 850 mmm per year.
    """

    self.RfactorYEAR = ifthenelse(Pannual <= 850, pow(0.04830*Pannual,1.1610), 587.8-1.219*Pannual+pow(Pannual,2))

    self.report(self.RfactorYEAR, os.path.join(outputdir,'Rannual'))

    print 'annual R-factor successfully computed!'

    return

def calc_Rmonthly(self, Precip, Pannual, RfactorYEAR, timeStep, outputdir):

    """
    The a priori computed annual R-factor is downscaled to monnthly values based on the monthly precipitation.
    """

    Precip_fract = cover(Precip/Pannual,scalar(0))
    self.RfactorMONTH = RfactorYEAR*Precip_fract

    self.report(self.RfactorMONTH, os.path.join(outputdir, 'R_month'))

    print 'monthly R-factor successfully computed for timestep', timeStep

    return
