# -*- coding: utf-8 -*-
"""
Created on Fri May 08 21:26:49 2015

@author: hoch
"""

import os

from pcraster import *

def Manning_n1(self, slopeGRA, Manning_n1_TBL):

    self.Manning_n1 = lookupscalar(Manning_n1_TBL,slopeGRA)

    return

def Manning_v3(self, NDVI):

    self.Manning_v3 = ifthenelse(NDVI < 0, 0, NDVI)

    return

def Manning_n4(self, barrensparse,closedshrub,crop,decbroad,decneedle,evergrbroad,evergrneedle,grass,mixforests,
              openshrub,permanentwet,savannas,snowice,urban,water,woodysavannas):

    Bar=barrensparse*0.0113;
    ShrubCl=closedshrub*0.4;
    Crp=crop*0.04;
    DecBrd=decbroad*0.36;
    DecNdl=decneedle*0.36;
    EgrBrd=evergrbroad*0.32;
    EgrNdl=evergrneedle*0.32;
    Grs=grass*0.368;
    For=mixforests*0.4;
    ShrubOp=openshrub*0.4;
    Wet=permanentwet*0.086;
    Sav=savannas*0.368;
    SnI=snowice*0.00001;
    Urb=urban*0.015;
    Wtr=water*0.023;
    SavWdy=woodysavannas*0.365;

    self.Manning_n4=Bar+ShrubCl+Crp+DecBrd+DecNdl+EgrBrd+EgrNdl+Grs+For+ShrubOp+Wet+Sav+SnI+Urb+Wtr+SavWdy

    return

def calc_Manning(self, Manning_n1, Manning_n2, Manning_v3, Manning_n4, timeStep, outputdir):

    self.Manning = Manning_n1+Manning_n2+Manning_v3*Manning_n4

    self.report(self.Manning, os.path.join(outputdir, 'Manning'))

    print 'Mannings coefficient is computed for timestep', timeStep

    return
