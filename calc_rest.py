# -*- coding: utf-8 -*-
"""
Created on Tue Mar 15 14:39:25 2016

@author: jannis
"""

import os
import pcraster as pcr

def calc_LS(self, slopeGRA, gamma_TBL, slopeLength, outputdir):

    """
    The topograhpic factor LS is computed. It consists of slope length factor L and slope steepness factor S.
    Both depend on the surface slope expressed in m/m for L and degree for S.
    """
    slopeDEG = pcr.atan(slopeGRA)
    slopeGRA = slopeGRA*100

    gamma = pcr.lookupscalar(gamma_TBL,slopeGRA)

    #L = slopeLength/22.13)I**gamma
    L = pow(slopeLength/22.13, gamma)
    S = -1.5+(17/(1+pcr.exp(2.3-(6.1*pcr.scalar(slopeDEG)))))
    self.LS = L*S

    self.report(self.LS,os.path.join(outputdir, 'LSFactor'))

    print 'LS-factor successfully computed!'

    return

def calc_C(self, theta, zeta, NDVI, timeStep, outputdir):

    """
    The C-factor reprsents the influence of vegetation on soil loss.
    More vegetation, i.e. higher NDVI, lead to lower C-values.
    """

    self.CfactorMONTH = pcr.exp(-theta*(NDVI/(zeta-NDVI)))
    self.CfactorMONTH = pcr.max(0,self.CfactorMONTH)
    self.CfactorMONTH = pcr.min(1,self.CfactorMONTH)

    self.report(self.CfactorMONTH, os.path.join(outputdir, 'C_month'))

    print 'monthly C-factor successfully computed for timestep', timeStep

    return

def calc_P(self, slopeGRA, apply_Pfactor='false'):

    """
    P stands for the impact of humans on the soil erosion and transport system.
    Due to lack of data, this values if often set to 1.
    A coarse estimation can be made with the Wener method (Wener, 1984) whcih relates
    P to the surface slope.
    """

    if self.apply_Pfactor == 'false':
        self.PfactorMONTH = 1
    if self.apply_Pfactor == 'true':
        self.PfactorMONTH = 0.2 + 0.3*slopeGRA/100

    return

def calc_HydroCoeff(self, Qsurface, Precip, timeStep, outputdir):

    HydroCoeff = Qsurface / Precip
    HydroCoeff = pcr.cover(HydroCoeff,0.0001)
    HydroCoeff = pcr.max(0,HydroCoeff)
    self.HydroCoeff = pcr.min(1,HydroCoeff)

    self.report(self.HydroCoeff, os.path.join(outputdir, 'HydrCoef'))

    print 'Hydrologic Coefficient is computed for timestep', timeStep

    return

def calc_SoilLoss(self, LS, RfactorMONTH, KfactorMONTH, CfactorMONTH, PfactorMONTH, CellArea, timeStep, outputdir):

    self.SoilLoss = LS*RfactorMONTH*KfactorMONTH*CfactorMONTH*PfactorMONTH*(1E-6)*CellArea/10000

    self.report(self.SoilLoss, os.path.join(outputdir, 'SoilLoss'))

    print 'Soil Loss successfully computed for timestep', timeStep

    return

def calc_DeliveryRatio(self, alpha, beta,  HydroCoeff, slopeGRA, Manning, slopeLength, timeStep, outputdir):

    DeliveryRatio= alpha*pow((HydroCoeff*pcr.sqrt(slopeGRA))/(Manning*slopeLength),beta)
    DeliveryRatio = pcr.max(0,DeliveryRatio)
    self.DeliveryRatio = pcr.min(1,DeliveryRatio)

    self.report(self.DeliveryRatio, os.path.join(outputdir, 'DelRatio'))

    print 'Delivery Ratio is computed for timestep', timeStep

    return

def calc_SedTransport(self, SoilLoss, DeliveryRatio, ErosionTOT, LDD, accufraction, accufractionflux, timeStep, outputdir):

    Erosion = SoilLoss*DeliveryRatio
    self.report(Erosion, os.path.join(outputdir, 'Erosion'))
    ErosionTOT += Erosion
    self.report(ErosionTOT, os.path.join(outputdir, 'Eros_TOT'))
    print 'Erosion is computed for timestep', timeStep

    if accufractionflux == 'false':
        SedTransport = pcr.accuflux(LDD, Erosion)
        self.report(SedTransport, os.path.join(outputdir, 'ST_flux'))
        print 'Sediment is routed with ACCUFLUX for timestep', timeStep

    if accufractionflux == 'true':
        SedTransport = pcr.accufractionflux(LDD, Erosion, accufraction)
        self.report(SedTransport, os.path.joni(outputdir, 'ST_frflux'))
        print 'Sediment is routed with ACCUFRACTIONFLUX for timestep', timeStep

    return
