# -*- coding: utf-8 -*-
"""
Created on Fri May 08 2015

PCR-GLOBST
THis model computes sediment transport into the oceans globally at 30' resolution.
It employs the RUSLE approach and hence models long-term results with monthly temporal resolution.
This is as well represented by the use of long-term monthly averages.

The model consists of three main parts:
First, soil loss calculated by means of RUSLE.
Second, delivery ratio of eroded soil.
Third, erosion and sediment transport into the oceans. This is achieved multiplying soil loss and delivery ratio and subsequently routing it along a LDD.

@author: J.M. Hoch, Departement of Physical Geography, Utrecht University, The Netherlands
         former: Departement of Civil Engineering and Management, University of Twente, The Netherlands
"""

from netCDF4 import Dataset
from pcraster import *
#from pcraster import ncConverter as ncConverter
from pcraster.framework import *
import os, sys
#import matplotlib.pyplot as plt
import numpy as np
import calc_Manning as Manning
import calc_Kfactor as Kfactor
import calc_Rfactor as Rfactor
import calc_rest
import load_data as load_data

#workdir = r'D:/hoch/Desktop/PhD/PCR-GLOBST/'
workdir = os.getcwd()
inputdir = os.path.join(workdir, 'input')
outputdir = os.path.join(workdir, 'output')


class RUSLE(DynamicModel):
  def __init__(self):
    DynamicModel.__init__(self)
    setclone(os.path.join(inputdir, 'globalclone.map'))

  def initial(self):

    ##-ROUTING OPTIONS
    #-if set to 'true', accufractionflux instead of accuflux will be executed
    self.accufractionflux = 'false'
    #-define fraction to be transported in accufractionflux (between 0 and 1)
    self.accufraction = 0.9

    ##-MODEL OPTIONS
    #-incorporate the P-factor as well or not?
    self.apply_Pfactor = 'false'

    #-setting initial value
    self.ErosionTOT = scalar(0)

    print 'INITIAL SECTION HAS BEEN STARTED'

    #-defining constants
    self.SMT = scalar(0)
    self.Manning_n2 = scalar(0.049)
    self.alpha = scalar(9.53)
    self.beta = scalar(0.79)
    self.theta = scalar(2)
    self.zeta = scalar(1)
    OM = scalar(1)
    print 'constants are defined'

    #-importing input data
    load_data.load_input_initial(self, inputdir, workdir)

    calc_rest.calc_LS(self, self.slopeGRA, self.gamma_TBL, self.slopeLength, outputdir)

    Kfactor.calc_Kannual(self, self.Texture0, self.Texture1, self.Texture2, self.Texture3, OM, self.Fclay, self.Fsand, self.Fsilt, outputdir)

    Rfactor.calc_Rannual(self, self.Pannual, outputdir)

    calc_rest.calc_P(self, self.slopeGRA)

    Manning.Manning_n1(self, self.slopeGRA, self.Manning_n1_TBL)

    Manning.Manning_n4(self, self.barrensparse,self.closedshrub,self.crop,self.decbroad,self.decneedle,self.evergrbroad,self.evergrneedle,
               self.grass,self.mixforests,
               self.openshrub,self.permanentwet,self.savannas,self.snowice,self.urban,self.water,self.woodysavannas)

    print 'INITIAL SECTION TERMINATED'

  def dynamic(self):

    timeStep=self.currentTimeStep()

    if timeStep == 1:
        print 'DYNAMIC SECTION HAS BEEN STARTED'
        if self.accufractionflux == 'false':
            print 'CHOSEN ROUTING FUNCTION: ACCUFLUX'
        if self.accufractionflux == 'true':
            print 'CHOSEN ROUTING FUNCTION： ACCUFRACTIONFLUX'
            print 'FRACTION: ',self.accufraction

    #-importing input map stacks for dynamic section
    load_data.load_input_dynamic(self, inputdir, workdir, timeStep)

    #-determining monthly Rfactor
    #-monthly R proportional to fraction of Pmonth to Pannual
    Rfactor.calc_Rmonthly(self, self.Precip, self.Pannual, self.RfactorYEAR, timeStep, outputdir)

    #-determining monthly K-factor
    Kfactor.calc_Kmonthly(self, self.Tmax, self.Tmin, self.SMT, self.KfactorRATIO, self.KfactorYEAR, timeStep, outputdir)

    #-computing C-factor based on equation of van Knijff
    calc_rest.calc_C(self, self.theta, self.zeta, self.NDVI, timeStep, outputdir)

    #-ultimately modelling monthly SOIL LOSS based on RUSLE
    calc_rest.calc_SoilLoss(self, self.LS, self.RfactorMONTH, self.KfactorMONTH, self.CfactorMONTH, self.PfactorMONTH, self.CellArea, timeStep, outputdir)

    #-calculating monthly Manning's surface roughness ceofficent N
    Manning.Manning_v3(self, self.NDVI)

    Manning.calc_Manning(self, self.Manning_n1, self.Manning_n2, self.Manning_v3, self.Manning_n4, timeStep, outputdir)

    #-determining the monthly Hydrologic Coefficient Hc
    calc_rest.calc_HydroCoeff(self, self.Qsurface, self.Precip, timeStep, outputdir)

    #-computing Delivery Ratio
    calc_rest.calc_DeliveryRatio(self, self.alpha, self.beta, self.HydroCoeff, self.slopeGRA, self.Manning, self.slopeLength, timeStep, outputdir)

    #-combining soil loss and delivery ratio to obtain monthly EROSION
    calc_rest.calc_SedTransport(self, self.SoilLoss, self.DeliveryRatio, self.ErosionTOT, self.LDD, self.accufraction, self.accufractionflux, timeStep, outputdir)

    #-providing time series at locations (not yet implemented)
#    SedTransportLOCS = timeoutput(self.RiverLOCS,SedTransport)
#    self.report(SedTransportLOCS,'SedTranspLOCS')

    if timeStep == 12:
        print 'HURRAY, MODEL IS FINISHED!!!'

nrOfTimeSteps= 12
myModel = RUSLE()
dynamicModel = DynamicFramework(myModel,nrOfTimeSteps)
dynamicModel.run()
