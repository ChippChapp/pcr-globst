# -*- coding: utf-8 -*-

from netCDF4 import Dataset

def write_nc(outputName, variableName):
    w_nc_fid = Dataset(outputName +'.nc', 'w', format='NETCDF4')
    w_nc_fid.description = 'bla'
    w_nc_fid.createDimension('time', None)
    w_nc_dim = w_nc_fid.createVariable('time', nc_fid.variables['time'].dtype,\
                                   ('time',))
    w_nc_fid.variables['time'][:] = time
    w_nc_var = w_nc_fid.createVariable(variableName, 'f8', ('time'))
    w_nc_var.setncatts({'long_name': u"mean Daily Air temperature",\
                    'units': u"degK", 'level_desc': u'Surface',\
                    'var_desc': u"Air temperature",\
                    'statistic': u'Mean\nM'})
    w_nc_fid.variables['air'][:] = air[time_idx, lat_idx, lon_idx]
    w_nc_fid.close()  # close the new file

    return
