# -*- coding: utf-8 -*-
"""
Created on Fri May 08 21:22:03 2015

@author: hoch
"""

from pcraster import *

def load_input_initial(self, inputdir, workdir):

    """
    All required files are loaded for the initial section, i.e. without time aspect
    """

    os.chdir(inputdir)

    self.slopeLength = readmap('globalbcat')
    self.LDD = readmap('LDD')
    self.CellArea = readmap('CellArea')
    self.slopeGRA = readmap('globalgradslope')
    self.RiverLOCS = readmap('rivers_maximumarea_selected')

    self.gamma_TBL = os.path.join(inputdir, 'gamma.txt')
    self.Manning_n1_TBL = os.path.join(inputdir, 'ManningN1.txt')

    self.Texture0 = readmap('Texture0_05deg')
    self.Texture1 = readmap('Texture1_05deg')
    self.Texture2 = readmap('Texture2_05deg')
    self.Texture3 = readmap('Texture3_05deg')
    self.Fsand = readmap('fsand_05deg')
    self.Fsand = self.Fsand / 100
    self.Fsilt = readmap('fsilt_05deg')
    self.Fsilt = self.Fsilt / 100
    self.Fclay = readmap('fclay_05deg')
    self.Fclay = self.Fclay / 100

    self.Pannual = readmap('Pannual')

    self.barrensparse = readmap('BarrenSparse.map')
    self.closedshrub = readmap('ClosedShrub.map')
    self.crop = readmap('Croplands.map')
    self.decbroad = readmap('DecBroadleaf.map')
    self.decneedle = readmap('DecNeedleleaf.map')
    self.evergrbroad = readmap('EvergrBroadleaf.map')
    self.evergrneedle = readmap('EvergrNeedle.map')
    self.grass = readmap('Grasslands.map')
    self.mixforests = readmap('MixedForests.map')
    self.openshrub = readmap('OpenShrub.map')
    self.permanentwet = readmap('PermanentWet.map')
    self.savannas = readmap('Savannas.map')
    self.snowice = readmap('SnowIce.map')
    self.urban = readmap('UrbanBuilt.map')
    self.water = readmap('Waterbodies.map')
    self.woodysavannas = readmap('WoodySavannas.map')

    os.chdir(workdir)

    print 'input files for initial section are loaded'

    return

def load_input_dynamic(self, inputdir, workdir, timeStep):

    """
    All required files are loaded for the dynamic section, i.e. variables with monthly variations
    """

    os.chdir(inputdir)

    self.Precip = self.readmap('Precip00')
    self.Tmax = self.readmap('Tmax05dg')
    self.Tmax = self.Tmax / 10
    self.Tmin = self.readmap('Tmin05dg')
    self.Tmin = self.Tmin /10
    self.NDVI = self.readmap('NDVI2009')
    self.NDVI = self.NDVI / 10000
    self.Qsurface = self.readmap('Qsurface')
    self.Qsurface = self.Qsurface*1000

    os.chdir(workdir)

    print 'input data is loaded for timestep', timeStep

    return
